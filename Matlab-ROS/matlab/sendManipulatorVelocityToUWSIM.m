function [ ] = sendManipulatorVelocityToUWSIM(udpSocket,jointName,jointVelocity)
%converts input to string and sends to UWSIM using udpSocket 
sprintfString = "%s,%i,";

if length(jointName) ~= length(jointVelocity)
    disp("Error: Number of joint state differs from number of Joint Name");
    return;
end

for i=1:1:length(jointName)
    sprintfString = strcat(sprintfString, "%s,");    
end

for i=1:1:(length(jointVelocity)-1)
    sprintfString = strcat(sprintfString, "%f,");    
end
sprintfString = strcat(sprintfString, "%f"); 

%disp(sprintfString);
sendString = sprintf(sprintfString,'JointStateVelocity',length(jointName), jointName, jointVelocity);
fwrite(udpSocket,sendString);

end
