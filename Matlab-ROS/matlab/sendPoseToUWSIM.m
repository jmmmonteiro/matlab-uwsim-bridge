function [] = sendPoseToUWSIM( udpSocket,input )
%converts input to string and sends to UWSIM using udpSocket 

sendString = sprintf("%s,%f,%f,%f,%f,%f,%f", 'Pose',input);
fwrite(udpSocket,sendString);

end

