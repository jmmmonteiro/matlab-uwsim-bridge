clear
clc

%create udp object
udpSocket = udp('127.0.0.1',4022);
%connect to udp
fopen(udpSocket)
message= [1.44,1,1,0,0,0];
jointState = [1,1,1]
jointVelocity = [0.5,0.5,0.5]
jointName = ["ARM_joint_1","ARM_Joint_2","ARM_Joint_3"];  
while(1)
    sendPoseToUWSIM(udpSocket, message); 
    %sendManipulatorStateToUWSIM(udpSocket, jointName, jointState); 
    sendManipulatorVelocityToUWSIM(udpSocket, jointName, jointVelocity); 
    message(1) = message(1) + 1;
    pause(0.1);
    %disp(message);
end