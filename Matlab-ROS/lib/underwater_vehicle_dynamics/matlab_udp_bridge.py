#!/usr/bin/env python2
# license removed for brevity

# Basic ROS imports
import re
import socket
import math
import pickle
import roslib 
roslib.load_manifest('underwater_vehicle_dynamics')
import rospy
import PyKDL
import sys


# import msgs
from std_msgs.msg import Float64MultiArray 
from geometry_msgs.msg import Pose 
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState 

#import services
from std_srvs.srv import Empty

# More imports
from numpy import *
import tf


class ROS_UDP_bridge:



    def initUDP( self ):

        # Configure sending socket
      try:
          self.socket_sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
          print 'Socket created'
      except socket.error, msg:
          print 'Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
          sys.exit()

          # Configure receiving socket
      try:
          self.socket_receiver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
          print 'Socket created'
      except socket.error, msg:
          print 'Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
          sys.exit()

      # Bind socket to local host and port
      try:
          self.socket_receiver.bind(('', self.receive_port))
      except socket.error, msg:
          print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]

      print 'Socket bind complete. Listen port=' + str(self.receive_port) + " send port=" + str(self.send_port)    


    def ReceivePoseUDP(self, dataTmp):
      #array to publish pose
      pose = []
      #create array of floats using list of strings
      pose = asarray(dataTmp)
      pose = pose.astype(float)

      self.pubVehiclePose(pose) ## updates the vehicle position 

    def ReceiveManipulatorStateUDP(self, dataTmp):
      #array to publish pose
      joint_state = []
      joint_name = []
      #get number of joints
      numberOfJoints = int( dataTmp[0] )

      for i in range( 0, numberOfJoints ):#joint state Message: [ numberOfJoints, jointName[numberofJoints], jointState[numberofJoints] ]
        #print i
        joint_name.append( dataTmp[ i + 1 ] ) # get joint names 
        #print dataTmp[ i + 1 ]
        joint_state.append( float( dataTmp [ i + 1 + numberOfJoints ] ) ) #get joint State values
      
      self.pubManipulatorState( joint_name, joint_state ) ## updates the vehicle position 

    def ReceiveManipulatorVelocityUDP(self, dataTmp):
      joint_velocity = []
      joint_name = []
      #get number of joints
      numberOfJoints = int( dataTmp[0] )

      for i in range( 0, numberOfJoints ):#joint state Message: [ numberOfJoints, jointName[numberofJoints], jointState[numberofJoints] ]
        #print i
        joint_name.append( dataTmp[ i + 1 ] ) # get joint names 
        #print dataTmp[ i + 1 ]
        joint_velocity.append( float( dataTmp [ i + 1 + numberOfJoints ] ) ) #get joint State values
      
      self.pubManipulatorVelocity( joint_name, joint_velocity ) ## updates the vehicle position 

    def ReceiveUDP(self):

      data, addr = self.socket_receiver.recvfrom(1024)
      #split received string in list of strings using delimeter ,
      dataTmp = data.split(",")
      # diffrent procedures according to type of message
      typeMessage = dataTmp[0]
      # removes message header 
      dataTmp.pop(0)

      if (typeMessage == 'Pose'):
        self.ReceivePoseUDP(dataTmp)
      elif (typeMessage == 'JointStatePosition'):
        self.ReceiveManipulatorStateUDP(dataTmp)
      elif (typeMessage == 'JointStateVelocity'):
        self.ReceiveManipulatorVelocityUDP(dataTmp)
      else:
         print 'Error: Type of message not supported'


    def pubVehiclePose(self, message):

      pose = Pose()

      pose.position.x = message[0]
      pose.position.y = message[1]
      pose.position.z = message[2]

      orientation = tf.transformations.quaternion_from_euler(message[3], message[4], message[5], 'sxyz')
      pose.orientation.x = orientation[0]
      pose.orientation.y = orientation[1]
      pose.orientation.z = orientation[2]
      pose.orientation.w = orientation[3]

      self.pub_pose.publish(pose)
      #self.rate.sleep()

      # Broadcast transform
      br = tf.TransformBroadcaster()
      br.sendTransform((message[0], message[1], message[2]), orientation, 
      rospy.Time.now(), "world", str(self.frame_id))

    def pubManipulatorState(self, jointName, jointState):
      #publish joint velocities
      msg = JointState()
      msg.name = jointName     #change to the appropriated joint names
      msg.position = jointState
      self.pub_joint.publish(msg)

    def pubManipulatorVelocity(self, jointName, jointVelocity):
      #publish joint velocities
      msg = JointState()
      msg.name = jointName     #change to the appropriated joint names
      msg.velocity = jointVelocity
      self.pub_joint.publish(msg)
     
    def iterate(self): ## Main loop operations
      #if publishPose == True:
      #self.ReceivePoseUDP()  ## waits until receives the UDP mesage and then:
      
      #if publishManipulatorState == True:  
      #self.ReceiveManipulatorStateUDP()  ## waits until receives the UDP mesage and then:

      #if publishManipulatorVelocity == True:
      #self.ReceiveManipulatorVelocityUDP()  ## waits until receives the UDP mesage and then:

      self.ReceiveUDP()
        

    def __init__(self):

      #print len(sys.argv)
      


      if len(sys.argv) != 7: 
        sys.exit("Usage: "+sys.argv[0]+" <namespace> <joint_state_command> <joint_state> <vehicle_pose>")
      

      #print
      ## kind of defines
      self.namespace = sys.argv[1]
      self.joint_command_topic = sys.argv[2]
      self.joint_state = sys.argv[3]
      self.vehicle_state = sys.argv[4]
      self.frame_id = self.namespace + "_base_link"
      self.external_force_topic = "/" + self.namespace + "/external_force"


      ## define publishers and subscribers
      self.pub_pose = rospy.Publisher(self.vehicle_state, Pose, queue_size=1)
      self.pub_joint = rospy.Publisher(self.joint_command_topic, JointState, queue_size=1) ## publisher for manipulator
      rospy.init_node('UDP_bridge')

      #rospy.wait_for_service('/dynamics/reset')
      #reset = rospy.ServiceProxy('/dynamics/reset', Empty) 

      ## other variables       
      self.receive_port = 4022               # Arbitrary non-privileged port
      self.send_port = 3533                  # Arbitrary non-privileged port
      self.matlab_ip_address = '127.0.0.1'   # Symbolic name meaning all available interfaces
      self.socket_sender =  socket.socket()  # 
      self.socket_receiver = socket.socket() # 


      self.initUDP()  #init sockets 





if __name__ == '__main__':

    try:
        ROS_bridge = ROS_UDP_bridge() 
        while not rospy.is_shutdown():
            ROS_bridge.iterate()

    except rospy.ROSInterruptException: pass
