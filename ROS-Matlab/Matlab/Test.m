clear
clc

udpr = dsp.UDPReceiver('LocalIPPort',3533);
setup(udpr);

i=1;
while(1)
    dataReceived = udpr();
    data = receiveROSdata(dataReceived);
    if data~='Error'
        pose(:,i) = data;
        i=i+1;
    end

end