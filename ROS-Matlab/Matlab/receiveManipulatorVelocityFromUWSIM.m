function [ jointName,jointStateVelocity] = receiveManipulatorVelocityFromUWSIM( manipulatorData )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
n = length(manipulatorData);
jointName = manipulatorData(1:n/2);
jointStateVelocity = str2double( manipulatorData(n/2+1:n) );

end