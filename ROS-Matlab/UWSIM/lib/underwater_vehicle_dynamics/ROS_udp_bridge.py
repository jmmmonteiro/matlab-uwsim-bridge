#!/usr/bin/env python2
# license removed for brevity

# Basic ROS imports
import re
import socket
import math
import pickle
import roslib 
roslib.load_manifest('underwater_vehicle_dynamics')
import rospy
import PyKDL
import sys


# import msgs
from std_msgs.msg import Float64MultiArray 
from geometry_msgs.msg import Pose 
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState 

#import services
from std_srvs.srv import Empty

# More imports
from numpy import *
import tf


class ROS_UDP_bridge:



    def initUDP(self):

      # Configure sending socket
      try:
          self.socket_sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
          print 'Socket created'
      except socket.error, msg:
          print 'Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
          sys.exit()

      # Bind socket to local host and port
      #try:
      #    self.socket_sender.bind(( socket.gethostname() , self.send_port ))
      #except socket.error, msg:
      #    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]

      #print 'Socket bind complete.  Send port =' + str(self.send_port)    



    def sendUDP(self, data):
      #print data
      
      self.socket_sender.sendto( data , ( '127.0.0.1' , self.send_port ) )
      #flag = self.socket_sender.sendall(data)
      #if flag <= 0:
        #print 'Error: Sending socket message' 
     
    def getTopicData(self, data):

      if self.topic_type == 'Pose':
        
        # convert from quaternions to euler angles
        quaternion = (data.orientation.x, data.orientation.y, data.orientation.z, data.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)

        # organize data structer
        position_x = data.position.x
        position_y = data.position.y
        position_z = data.position.z
        orientation_roll = euler[0]*180/3.14 # roll in degrees
        orientation_pitch = euler[1]*180/3.14 # pitch in degrees
        orientation_yaw = euler[2]*180/3.14 # yaw in degrees
        #organize data structer in array of floats
        Temp_data_float = array(['Pose',position_x, position_y, position_z, orientation_roll, orientation_pitch, orientation_yaw])
        
        #convert data structer float array into string array
        Temp_data_string = array2string( Temp_data_float, separator=',')
        


      elif self.topic_type == 'JointStatePosition':

        #organize data structer in array of floats
        Temp_data_float = data.position 

        Temp_data_float.insert(0,"JointStatePosition")#CONFIRMAR SE ESTA A INSERIR BEM
        #convert data structer float array into string array
        Temp_data_string = array2string(Temp_data_float, separator=',')
        
        

      elif self.topic_type == 'JointStateVelocity':

        #organize data structer in array of floats
        Temp_data_float = data.velocity 

        Temp_data_float.insert(0,"JointStateVelocity")#CONFIRMAR SE ESTA A INSERIR BEM
        #convert data structer float array into string array
        Temp_data_string = array2string(Temp_data_float, separator=',')
        

      else:
       print 'Error: Type of message not supported'

      #send data over UDP
      self.sendUDP(Temp_data_string)


    def __init__(self):



      if len(sys.argv) != 7: 
        sys.exit("Usage: "+sys.argv[0]+" <namespace> <joint_state_command> <joint_state> <vehicle_pose>")
      

      #print
      ## kind of defines
      self.namespace = sys.argv[1]
      self.input_topic = sys.argv[2]
      self.topic_type = sys.argv[3]
      self.node_name = sys.argv[4]

      print self.topic_type
      print 
      print

      ## define publishers and subscribers
      #rospy.Subscriber(self.input_topic, self.topic_type, self.getTopicData)                                   #subscribes topic /zarco/pose (type geometry_msgs/Pose)
      if self.topic_type == "Pose":
        rospy.Subscriber(self.input_topic, Pose, self.getTopicData)                                   #subscribes topic /zarco/pose (type geometry_msgs/Pose)
      elif self.topic_type == "JointStatePosition":
        rospy.Subscriber(self.input_topic, JointStatePosition, self.getTopicData)                                   #subscribes topic /zarco/pose (type geometry_msgs/Pose)
      elif self.topic_type == "JointStateVelocity":
        rospy.Subscriber(self.input_topic, JointStateVelocity, self.getTopicData)                                   #subscribes topic /zarco/pose (type geometry_msgs/Pose)
      else:
        sys.exit("Topic Type not supported")

      rospy.init_node(self.node_name)

      #rospy.wait_for_service('/dynamics/reset')
      #reset = rospy.ServiceProxy('/dynamics/reset', Empty) 

      ## other variables       
      self.send_port = 3533                  # Arbitrary non-privileged port
      self.matlab_ip_address = '127.0.0.1'   # Symbolic name meaning all available interfaces
      self.socket_sender =  socket.socket()  # 

      self.initUDP()  #init sockets 



if __name__ == '__main__':

    try:
        ROS_bridge = ROS_UDP_bridge() 
        while not rospy.is_shutdown():
           #(ROS_bridge.socket_sender,addr) = ROS_bridge.socket_sender.accept()
           a=1

        self.socket_sender.close()

    except rospy.ROSInterruptException: pass